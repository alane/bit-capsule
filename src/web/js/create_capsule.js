
/* This is to hide the +add more email text field until you click +add more...*/
$(document).ready(function(){
	$("#add_more").click(function(){
    	if ($("#emails").children().length < 10){
            $("#emails").append('<div class="control-group">'+
                    '<div class="input-prepend">'+
                        '<span class="add-on"><i class="icon-envelope"></i></span>'+
                        '<input name="email" class="emails" type="text" placeholder="email address">'+
                    '</div>'+
                '</div>');
        } 
        if ($("#emails").children().length == 10){
            $("#add_more").hide();
        }
  	});
    var myDropzone = new Dropzone("#capsule_form", { 
        url: "/file/post", 
        maxFilesize: 1,
        previewsContainer: "#file_preview",
        clickable: ".well"
    });

  	// initialize text area resize  
  	$('textarea').autoResize();


	$('#calendar').hide();
	$('#dates').change(function() {
		var id = $(this).children(":selected").attr("id");
		if (id == "custom_date") {
			$('#calendar').show();
		} else {
			$('#calendar').hide();
		}
	});
    $( "#calendar" ).datepicker();
	$("#capsule_form").validate({
		rules: {
            "message": {
                required: true,
                maxlength: 8000
            },
    		"email": {
        		required: true,
        		email: true
    		},
            "emailAddOn": {
                email: true
            }
	    }, 
	    messages: {
	       "email": {
	            required: "Please enter email.",
	            email: "Please enter a valid email address."
            },
           "emailAddOn": {
                email: "Please enter a valid email address."
            }
	    },
        //to test that the emailAddOn validation is working
        /*submitHandler: function(form) {
            alert('sucessful submit');
        } */
	});
});


/* Date picker - Step one if custom date is selected, show the calendar input and the text input for the date. 
Then, if they select something not "custom date" in the drop down, hide the previously mentioned stuff. */
